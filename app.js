import { readdir } from 'node:fs/promises';

/*
const sharp = require('sharp')

console.log('Resizing')

sharp('alps.jpg')
  .resize(1200, 1200)
  .rotate(10, {background: "#ff0000"})
  .toFile('output.jpg', function(err) {
  });
*/

try {
  const photoRoot = await readdir('photos', { withFileTypes: true });
  for (const photoDirent of photoRoot) {
    // photoDirent je zaznam o souboru uvnitr photos/
    if (!photoDirent.isDirectory()) {
      // photoDirent neni adresar = nezajima nas
      continue
    }
    // ted je photoDirent adresar
    console.log(`Nasel jsem adresar ${photoDirent.name}`)
    
    // Jednotliva galerie
    const photoDir = await readdir(`photos/${photoDirent.name}`)

    for (const photoName of photoDir) {
      if (photoName.toLowerCase().endsWith('.jpg')) {
        console.log(` photos/${photoDirent.name}/${photoName}`)
      }
    }

  }
} catch (err) {
  console.error(err);
} 